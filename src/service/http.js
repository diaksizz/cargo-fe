import axios from "axios"
export const request = async(endpoint, method, data='') => {
    try {
        const token = localStorage.getItem('JWT_LOGIN')
        const url = "https://oktakun.my.id/api/" + endpoint
        const headers = {
            'Authorization': 'Bearer ' + token
        }
        console.log(headers)
        const res = await axios.request({ url: url, headers: headers, data: data, method: method })
        if (res.status === 200) {
            return {
                'data': res.data,
                'status': true
            }
        }
        else {
            return {
                'data': res.statusText,
                'status': false
            }
        }
    }
    catch(err) {
        return err
    }
} 

export const Authentication = async (data) => {
    try {
        const url = "https://oktakun.my.id/api/login/"
        const res = await axios.post(url, data)
        if (res.status == 200) {
            
            localStorage.setItem('JWT_LOGIN', res.data.access)
            localStorage.setItem('JWT_LOGIN_REFRESH', res.data.refresh)
            return {
                'status': true,
                'data': res.data
            }
        }
        else {
            return {
                'status': false,
                'error': res.data
            }
        }
    }
    catch(err) {
        return err
    }
}


export const Registration = async (data) => {
    try {
        const url = "https://oktakun.my.id/api/register/"
        const res = await axios.post(url, data)
        if (res.status == 201) {
            return {
                'status': true,
                'data': res.data
            }
        }
        else {
            return {
                'status': false,
                'error': res.data
            }
        }
    }
    catch(err) {
        return err
    }
}
